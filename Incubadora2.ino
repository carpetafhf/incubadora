//#include "DHT.h"
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
//#include "Adafruit_BME280.h"
#include "cactus_io_BME280_I2C.h"

//#define DHTPIN 8     // what digital pin we're connected to
#define RelePIN 12
#define BotonPIN 8
#define PotencioPIN A3
#define AlarmaPIN 4
#define tiempoDelay 50
#define bucleRefreshLCD 100
#define saltaAlarma 12000
#define timeoutConfig 4000

LiquidCrystal_I2C lcd(0x27,16,2); // set the LCD address to 0x27 for a 16 chars and 2 line display

//BME280_I2C bme; // I2C using address 0x77
BME280_I2C bme(0x76); // I2C using address 0x76

// Uncomment whatever type you're using!
//#define DHTTYPE DHT11   // DHT 11
#define DHTTYPE DHT22   // DHT 22  (AM2302), AM2321
//#define DHTTYPE DHT21   // DHT 21 (AM2301)

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
//DHT dht(DHTPIN, DHTTYPE);

int BotonPulsado;
int EstadoConfig;
int tiempoConfig;
int bucleLCD;
float errorhum;


bool AlarmEnabled; 

float rangodebajo;
float rangoalto;
int intervalo;

float minimoinicial;
float maximoinicial;

int alarma;

void setup() {
  Serial.begin(9600);

  pinMode(RelePIN,OUTPUT);
  pinMode(BotonPIN,INPUT);
  pinMode(AlarmaPIN,OUTPUT);

  lcd.init(); //initialize the lcd
  lcd.backlight(); //open the backlight

  if (!bme.begin()) 
  {
    lcd.print("ERROR sensor temperatura" );
    while (1);
  }
  
  
//  dht.begin();

  BotonPulsado = 0;
  EstadoConfig = 0;
  tiempoConfig=0;
  bucleLCD=0;
  errorhum = 6.0; 
  bme.setTempCal(-1.5);// Temp was reading high so subtract 1.5 degrees

  minimoinicial = 37.0;
  maximoinicial = 38.0;
  intervalo = 4;

  rangodebajo = 37.0;
  rangoalto = 38.0;

  alarma = 0;
  AlarmEnabled = false;
}

/*char* string2char(String command)
{
    if(command.length()!=0){
        char *p = const_cast<char*>(command.c_str());
        return p;
    }
}*/


void loop() 
{
  Serial.println(EstadoConfig);
  
  if (digitalRead(BotonPIN) == LOW)     //Pregunta si el pulsador está presionado
  {
    BotonPulsado = 1;
    EstadoConfig++;
    if(EstadoConfig==4) 
    {
      lcd.setCursor(0,0);
      lcd.print("                " );
      lcd.setCursor(0,1);
      lcd.print("                " );
      EstadoConfig = 0;
      alarma = 0;
    }
    while(digitalRead(BotonPIN) == LOW)
    {
    }
    
  }

  if(EstadoConfig==0)
  {

      bme.readSensor();

      float t = bme.getTemperature_C();
      float h = bme.getHumidity();
      /*Serial.print(bme.getPressure_MB()); Serial.print(" mb\t"); // Pressure in millibars
      Serial.print(bme.getHumidity()); Serial.print(" %\t\t");
      Serial.print(bme.getTemperature_C()); Serial.print(" *C\t");
      Serial.print(bme.getTemperature_F()); Serial.println(" *F");*/
    
      // Check if any reads failed and exit early (to try again).
      if (isnan(h) || isnan(t)) 
      {
        //Serial.println("Error al leer del sensor de temperatura");
        return;
      }
    
      // Compute heat index in Celsius (isFahreheit = false)
      //float hic = dht.computeHeatIndex(t, h, false);
    
      /*Serial.print("Humidity: ");
      Serial.print(h);
      Serial.print(" %\t");
      Serial.print("Temperature: ");
      Serial.print(t);
      Serial.print(" *C ");
      Serial.print("Heat index: ");
      Serial.print(hic);
      Serial.println(" *C ");*/

      bucleLCD++;

      if(bucleLCD > bucleRefreshLCD)
      {
        lcd.setCursor(0,0);
        lcd.print("TEMP: " ); lcd.print(t,1);
        lcd.setCursor(0,1);
        lcd.print("HUMEDAD: " ); lcd.print(h+errorhum,1);
        bucleLCD = 0;
      }
      //Serial.println(alarma);

      if(alarma > saltaAlarma)
      {
        digitalWrite(AlarmaPIN,HIGH);
      }
      else
      {
        if(alarma == 0) 
        {
          digitalWrite(AlarmaPIN,LOW);
        }
      }
       
      if(t < rangodebajo) 
      {
        digitalWrite(RelePIN, LOW);
        if(AlarmEnabled) alarma++;
      }
      else
      {
        if(t > rangoalto)
        {
          digitalWrite(RelePIN,HIGH);
          if(AlarmEnabled) alarma++;
        }
        else
        {
          alarma = 0;
        }
      }
      
      
  }
  else
  {
    int valor = analogRead(PotencioPIN);    

    if(EstadoConfig==1)
    {
       alarma=0;
       rangoalto = maximoinicial + (float) valor * intervalo /1024;
       Serial.println(rangoalto);
       lcd.setCursor(0,0);
       lcd.print("TMAX: " ); lcd.print(rangoalto,2);
       lcd.setCursor(0,1);
       lcd.print("              " );
       
    }
    else
    {
       if(EstadoConfig==2)
       {
         alarma=0;
         rangodebajo = minimoinicial - (float) valor * intervalo /1024;
         lcd.setCursor(0,0);
         lcd.print("TMIN: " ); lcd.print(rangodebajo,2);
         lcd.setCursor(0,1);
         lcd.print("              " );
       }
       else
       {
         alarma=0;
         float lectura = (float) valor;
         lcd.setCursor(0,0);
         if(lectura < 512) 
         {
          lcd.print("ALARMA: desactivada" );
          AlarmEnabled = false;
         }
         else
         {
           lcd.print("ALARMA: activada");
           AlarmEnabled = true;
         }
         lcd.setCursor(0,1);
         lcd.print("              " );
         
         bucleLCD = bucleRefreshLCD;
       }
       
    }
    tiempoConfig++;
    if(tiempoConfig>timeoutConfig)
    {
      EstadoConfig=0;
    }
  }
  delay(tiempoDelay);
}
